
import { combineReducers } from "redux";
import messages from "../containers/Chat/reducer";
import modal from "../containers/Modal/reducer";

const rootReducer = combineReducers({
  messages,
  modal
});

export default rootReducer;

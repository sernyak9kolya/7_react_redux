import React from 'react';
import propTypes from 'prop-types';
import moment from 'moment';
import Message from '../Message/Message';
import Divider from '../Divider/Divider';
import './MessageList.css';

class MessageList extends React.Component {
  constructor(props) {
    super(props);

    this.messageListRef = React.createRef();
  }
  calendarFormat = {
    lastDay : '[Yesterday]',
    sameDay : '[Today]',
    lastWeek : '[last] dddd',
    sameElse : 'L'
  };

  scrollToBottom() {
    const messageListEl = this.messageListRef.current;
    messageListEl.scrollTop = messageListEl.scrollHeight;
  }

  componentDidMount() {
    this.scrollToBottom()
  }
  
  componentDidUpdate() {
    this.scrollToBottom()
  }

  render() {
    const { messages } = this.props;
    const messagesGroupByDate = {};
    const messagesEl = [];

    const messageEl = message => <Message key={message.id} message={message} />;
    const dividerEl = label => <Divider key={label} label={label} />;

    messages.forEach(message => {
      const calendarDate = moment(message.createdAt).calendar(this.calendarFormat);

      if (messagesGroupByDate[calendarDate]) {
        messagesGroupByDate[calendarDate].push(message);
      } else {
        messagesGroupByDate[calendarDate] = [message];
      }
    });

    for (let day in messagesGroupByDate ) {
      messagesEl.push(messagesGroupByDate[day].map(messageEl));
      if (day === 'Today') continue;
      messagesEl.push(dividerEl(day));
    }

    return (
      <div className="d-flex flex-column messageList" ref={this.messageListRef}>
        {messagesEl}
      </div>
    );
  };
}

MessageList.propTypes = {
  messages: propTypes.arrayOf(propTypes.object)
};

export default MessageList;

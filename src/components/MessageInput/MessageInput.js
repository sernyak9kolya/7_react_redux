import React from 'react';
import { connect } from 'react-redux';
import propTypes from 'prop-types';
import { v4 as uuidv4 } from 'uuid';
import { addMessage } from '../../containers/Chat/actions';
import { CURRENT_USER } from '../../config';
import './MessageInput.css';

class MessageInput extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      text: ''
    };
  }

  createMessage(e) {
    e.preventDefault();
    const { text } =this.state;

    if (!text) {
      return;
    }

    const newMessage = {
      text,
      id: uuidv4(),
      userId: CURRENT_USER.id,
      user: CURRENT_USER.name,
      avatar: CURRENT_USER.avatar,
      createdAt: new Date().toISOString(),
      editedAt: ''
    };

    this.props.addMessage(newMessage);

    this.resetInput();
  };

  handleInput = e => {
    const text = e.target.value.trimStart();
    this.setState(() => ({ text }));
  };

  resetInput = () => {
    this.setState(() => ({ text: '' }));
  };

  render() {
    const { text } = this.state;

    return (
      <form className="d-flex" onSubmit={e => this.createMessage(e)}>
        <input
          type="text"
          value={text}
          placeholder="Enter your message"
          className="messageInput form-control form-control-lg"
          onChange={this.handleInput}
        />
        <button type="submit" className="messageInput__btn btn btn-primary">
          Send <i className="fa fa-send"></i>
          </button>
      </form>
    );
  };
}

MessageInput.propTypes = {
  messages: propTypes.arrayOf(propTypes.object)
};

const mapDispatchToProps = {
  addMessage
};

export default connect(null, mapDispatchToProps)(MessageInput);


import React from 'react';
import propTypes from 'prop-types';
import './Divider.css';

class Divider extends React.Component {
  render() {
    const { label } = this.props;

    return (
      <div className="divider">{ label }</div>
    );
  };
}

Divider.propTypes = {
  label: propTypes.string
};

export default Divider;

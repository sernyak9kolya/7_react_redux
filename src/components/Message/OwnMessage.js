import React from 'react';
import { connect } from 'react-redux';
import propTypes from 'prop-types';
import moment from 'moment';
import { deleteMessage } from '../../containers/Chat/actions';
import { openModal, setCurrentMessageId } from '../../containers/Modal/actions';

class OwnMessage extends React.Component {
  editMessage = id => {
    const { openModal, setCurrentMessageId } = this.props;
    setCurrentMessageId(id);
    openModal();
  };

  deleteMessage = id => {
    const confirmed = window.confirm('Do you really want to delete the message?');

    if (!confirmed) {
      return;
    }

    this.props.deleteMessage(id);
  };

  render() {
    const { message } = this.props;
    const { text, createdAt } = message;

    return (
      <div className="message message-own align-self-end">
        <div className="message__content alert alert-light">
          <div className="message__text">{text}</div>
          <div className="message__createdAt">{moment(createdAt).format('HH:mm')}</div>
        </div>
        <div className="message__actions">
          <i className="message__icon fa fa-pencil" onClick={() => this.editMessage(message.id)}></i>
          <i className="message__icon fa fa-trash" onClick={() => this.deleteMessage(message.id)}></i>
        </div>
      </div>
    );
  };
}

OwnMessage.propTypes = {
  message: propTypes.object,
  deleteMessage: propTypes.func,
  openModal: propTypes.func,
  setCurrentMessageId: propTypes.func
};

const mapDispatchToProps = {
  deleteMessage,
  openModal,
  setCurrentMessageId
};

export default connect(null, mapDispatchToProps)(OwnMessage);

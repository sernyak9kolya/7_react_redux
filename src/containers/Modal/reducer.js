import { OPEN_MODAL, CLOSE_MODAL, SET_CURRENT_MSG_ID, DROP_CURRENT_MSG_ID } from './actionTypes';

const initialState = {
  isOpen: false,
  currentMessageId: null
};

export default function (state = initialState, action) {
  switch (action.type) {
    case OPEN_MODAL: {
      return {
        ...state,
        isOpen: true,
      };
    }

    case CLOSE_MODAL: {
      return {
        ...state,
        isOpen: false
      };
    }

    case SET_CURRENT_MSG_ID: {
      const { id } = action.payload;
      return {
        ...state,
        currentMessageId: id
      }
    }

    case DROP_CURRENT_MSG_ID: {
      return {
        ...state,
        currentMessageId: null
      }
    }

    default:
      return state;
  }
}

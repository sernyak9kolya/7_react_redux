import React from 'react';
import { connect } from 'react-redux';
import { openModal, closeModal, setCurrentMessageId, dropCurrentMessageId } from './actions';
import { updateMessage } from '../Chat/actions';
import './Modal.css';

class Modal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      text: ''
    };
  }

  onClose() {
    const { closeModal, dropCurrentMessageId } = this.props;
    closeModal();
    dropCurrentMessageId();
  }

  onSave() {
    const { text } = this.state;
    const { currentMessageId, updateMessage } = this.props;

    if (currentMessageId) {
      updateMessage({
        text,
        id: currentMessageId
      });
    }

    this.onClose();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.currentMessageId !== this.props.currentMessageId) {
      const message = this.props.messages.find(msg => msg.id === nextProps.currentMessageId);
      this.setState({ text: message?.text });
    }
  }

  render() {
    const { text } = this.state;
    const { isOpen } = this.props;

    const modalDisplayClass = isOpen ? 'd-block' : '';

    return (
      <div className={`modal ${modalDisplayClass}`} role="dialog">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">Edit Message</h5>
              <button type="button" className="close" aria-label="Close" onClick={() => this.onClose()}>
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <div className="form-group">
                <textarea
                  className="form-control"
                  placeholder="Message"
                  rows="5"
                  value={text}
                  onChange={e => this.setState({ text: e.target.value })} />
              </div>
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-outline-secondary" onClick={() => this.onClose()}>Close</button>
              <button type="button" className="btn btn-primary" onClick={() => this.onSave()}>Save</button>
            </div>
          </div>
        </div>
      </div>
    );
  };
}

const mapStateToProps = state => ({
  messages: state.messages,
  isOpen: state.modal.isOpen,
  currentMessageId: state.modal.currentMessageId
});

const mapDispatchToProps = {
  openModal,
  closeModal,
  setCurrentMessageId,
  dropCurrentMessageId,
  updateMessage
};

export default connect(mapStateToProps, mapDispatchToProps)(Modal);

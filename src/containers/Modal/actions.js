import { OPEN_MODAL, CLOSE_MODAL, SET_CURRENT_MSG_ID, DROP_CURRENT_MSG_ID } from './actionTypes';

export const openModal = () => ({
  type: OPEN_MODAL
});

export const closeModal = () => ({
  type: CLOSE_MODAL
});

export const setCurrentMessageId = id => ({
  type: SET_CURRENT_MSG_ID,
  payload: { id }
});

export const dropCurrentMessageId = () => ({
  type: DROP_CURRENT_MSG_ID
});

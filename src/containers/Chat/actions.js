import { SET_MESSAGES, ADD_MESSAGE, UPDATE_MESSAGE, DELETE_MESSAGE } from './actionTypes';

export const setMessages = messages => ({
  type: SET_MESSAGES,
  payload: { messages }
});

export const addMessage = message => ({
  type: ADD_MESSAGE,
  payload: { message }
});

export const updateMessage = message => ({
  type: UPDATE_MESSAGE,
  payload: { message }
});

export const deleteMessage = id => ({
  type: DELETE_MESSAGE,
  payload: { id }
});
